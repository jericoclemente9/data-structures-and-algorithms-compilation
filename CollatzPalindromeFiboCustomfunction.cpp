#include <iostream>
#include <cstdlib>

using namespace std;

void switcher();

class List{
	private:
		typedef struct node{
			int data;
			node* next;
		}* nodePtr;
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
	
	public:
		List();
		void AddNode(int addData);
		void DeleteNode(int delData);
		void PrintList();
		void EditNode(int data, int newData);
		int FindData(int Data);
		
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
}

void List::AddNode(int addData){
	nodePtr n = new node;
	n->next = NULL;
	n->data = addData;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
	}
	else{
		head = n;
	}
}

void List::DeleteNode(int delData){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	while (curr != NULL && curr->data != delData){
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL){
		cout<<delData<<" was not in the List\n";
		switcher();
	}
	else if (curr==head){
		head=head->next;
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
	}
	delete delPtr;
}

void List::PrintList(){
	curr = head;
	while (curr != NULL){
		cout<<curr->data<<endl;
		curr = curr->next;
	}
}
//at a specified index
void List::EditNode(int data, int newData){
	curr = head;
	while (curr->data != data){
		curr = curr->next;
	}
	if (curr){
		curr->data = newData;
	}
}

int List::FindData(int Data){
	curr = head;
	int currIndex = 1;
	while (curr && curr->data != Data){
		curr = curr->next;
		currIndex++;
	}
	if (curr){
		return currIndex ;
	}
	return 0;
}

int main();
List list;
int *collatz(int input)
{
	int counter = 0;
	static int arr[1500];
	while (input!=1){
		if (input%2!=0){
			input = input*3+1;
			arr[counter]=input;
		}
		else{
			input = input/2;
			arr[counter]=input;
		}
		counter++;
	}
	return arr;
}

int SumOdd(int *p)
{
	int sum = 0;
	static int oddArr[1500];
	for (int i=0; i<=1500; i++){
		if (*(p+i)%2!=0){		
			oddArr[i]=*(p+i);
		}
		if (oddArr[i]!=0){
			sum+=oddArr[i];
		}
	}
	return sum;
}

unsigned long long int SumOddArr(unsigned long long int arr[])
{
	unsigned long long int sum = 0;
	static unsigned long long int oddArr[1500];
	for (int i=0; i<=1500; i++){
		if (arr[i]%2!=0){		
			oddArr[i]=arr[i];
		}
		if (oddArr[i]!=0){
			sum+=oddArr[i];
		}
	}
	return sum;
}

int isPalin(int num)
{
	int opp=0, buff;
	buff = num;
	while (num>0){
		opp = opp*10+num%10;
		num = num/10;
	}
	if (buff==opp){
		return buff;
	}
	else{
		return 0;
	}
}

int *palin(int *a)
{
	int foo, boo;
	static int temp[1500];
	static int palind[1500];
	for (int i=0; i<=1500; i++){
		if (*(a+i)!=0){
			temp[i]=*(a+i);
		}
		if (temp[i]!=0){
			foo = temp[i];
			boo = isPalin(foo);
			palind[i]=boo;
		}
	}
	return palind;
}

unsigned long long int *fibonacci()
{
	static unsigned long long int fibo[90];
	const int n = 90;
	unsigned long long int t1=0, t2=1, next=0;
	for(int i=0; i<=n; ++i){
		if(i==0){
			fibo[i]=0;
		}
		else if(i==1){
			fibo[i]=1;
		}
		else{
			fibo[i]=fibo[i-1]+fibo[i-2];
		}
	}
	return fibo;
}

unsigned long long int *isCollatz(int arr[], unsigned long long int fibo[])
{
	static unsigned long long int temp[1500];
	for(int i=2; i<=90; i++){
		for(int j=1500; j>=0; j--){
			if (arr[j]==fibo[i]){
				temp[j]=arr[j];
				break;
			}
		}
	}
	return temp;
}
void switcher()
{
	cout<<"1) Add 2) Delete 3) Edit 4) View 5) Find 6) Generate new collatz: ";
	char c;
	cin>>c;
	cin.ignore(1,'\n');
	switch(c){
		case '1':{
			int data;
			cout<<"Enter number to add: ";
			cin>>data;
			cin.ignore(1,'\n');
			list.AddNode(data);
			cout<<"\nData added\n";
			switcher();
			break;
		}
		case '2':{
			list.PrintList();
			cout<<"Enter data to be deleted: ";
			int delData;
			cin>>delData;
			cin.ignore(1,'\n');
			list.DeleteNode(delData);
			cout<<"\nData deleted\n";
			switcher();
			break;
		}
		case '3':{
			cout<<"Enter data to be edited: ";
			int edit;
			cin>>edit;
			cout<<"\nEnter new data: ";
			int newData;
			cin>>newData;
			list.EditNode(edit,newData);
			cout<<"\nData edited\n";
			switcher();
			break;
		}
		case '4':{
			cout<<"Contents of the list:\n";
			list.PrintList();
			switcher();
			break;
		}
		case '5':{
			cout<<"Enter data to be searched: ";
			int find;
			cin>>find;
			int index = list.FindData(find);
			cout<<"The data "<<find<<" is in the "<<index<<" position"<<endl;
			switcher();
			break;
		}
	}
}

int main()
{
	int input, *p, *n, x, y;
	static unsigned long long int *f, z, *t;
	static unsigned long long int temparr[1500];
	static unsigned long long int temparr2[1500];
	static int colarr[1500];
	static unsigned long long int fibotemp[1500];
	cout<<"Input: ";
	cin>>input;
	cin.ignore(1,'\n');
	if(input==0){
		cout<<"Nothing will be generated!\n";
		main();
	}
	p = collatz(input);
	cout<<"Collatz:\n";
	for (int i=0; i<=1500; i++){
		if (*(p+i)!=0){
			cout<<*(p+i)<<", ";
			colarr[i]=*(p+i);
		}
	}
	x = SumOdd(p);
	cout<<"\nSum of odds in collatz is: "<<x<<endl;
	n = palin(p);
	cout<<"Palindromes: "<<endl;
	for (int i=0; i<=1500; i++){
		if (*(n+i)!=0){
			cout<<*(n+i)<<"; ";
			temparr[i]=*(n+i);
		}
	}
	y = SumOddArr(temparr);
	cout<<"\nSum of Odd in Palindrome is: "<<y<<endl;
	f = fibonacci();
	for(int i=0; i<=1500; i++){
		if(*(f+i)!=0){
			fibotemp[i]=*(f+i);
		}
	}
	z = SumOddArr(fibotemp);
	cout<<"\nSum of Odd in Fibonacci Sequence is: "<<z<<endl;
	t = isCollatz(colarr,fibotemp);
	for(int i=1500; i>=0; i--){
		if(*(t+i)!=0){
			temparr2[i]=*(t+i);
		}
		if(temparr2[i]!=0)
			list.AddNode(temparr2[i]);
	}
	cout<<"Contents of the list: "<<endl;
	list.PrintList();
	switcher();
}
