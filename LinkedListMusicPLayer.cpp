#include <iostream>
#include<cstdlib>

using namespace std;

void switcher();

class List{
	private:
		typedef struct node{
			string title;
			string singer;
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr tail;
	
	public:
		List();
		void AddNode(string addTitle, string addSinger);
		void DeleteNode(string delMusic);
		void PrintList();
		void EditNode(int index, string newMusic, string newSinger);
		int FindData(string music);
		string Playing(int index);
		
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
	tail = NULL;
}

void List::AddNode(string addTitle, string addSinger){
	nodePtr n = new node;
	n->next = NULL;
	n->title = addTitle;
	n->singer = addSinger;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
		tail = n->next;
	}
	else{
		head = n;
		tail = n;
	}
}

void List::DeleteNode(string delMusic){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	while (curr != NULL && curr->title != delMusic){
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL){
		cout<<delMusic<<" was not in the List\n";
		switcher();
	}
	else if (curr==head){
		head=head->next;
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
	}
	delete delPtr;
}

void List::PrintList(){
	curr = head;
	if(curr!=NULL){
		while (curr != NULL){
			cout<<"Title: "<<curr->title<<" by: "<<curr->singer<<endl;
			curr = curr->next;
		}
	}
	else{
		cout<<"Empty!\n";
		switcher();
	}
}

void List::EditNode(int index, string newMusic, string newSinger){
	curr = head;
	for (int currIndex = 1; currIndex <= index; currIndex++){
		curr = curr->next;
	}
	if (curr){
		curr->title = newMusic;
		curr->singer = newSinger;
	}
}

int List::FindData(string music){
	curr = head;
	int currIndex = 1;
	while (curr && curr->title != music){
		curr = curr->next;
		currIndex++;
	}
	if (curr){
		return currIndex;
	}
	else{
		return 0;
	}
}

string List::Playing(int index){
	nodePtr n;
	curr = head;
	int currIndex = 1, numnode=1;
	while(curr&&curr->next!=NULL){
		curr=curr->next;
		numnode++;
	}
	if(index==1){
		curr=head;
		cout<<"Previous: None\n";
		while(currIndex!=index+1){
			temp = curr;
			curr=curr->next;
			currIndex++;
		}
		cout<<"Playing: "<<temp->title<<endl;
		cout<<"Next: "<<curr->title<<endl;
	}
	else if(index==numnode){
		curr=head;
		while (currIndex != index&&curr->next!=NULL){
			temp = curr;
			curr = curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<endl;
		cout<<"Playing: "<<curr->title<<endl;
		cout<<"Next: None\n";
	}
	else{
		curr=head;
		while(currIndex!=index&&curr->next!=NULL){
			temp=curr;
			curr=curr->next;
			n=curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<endl;
		cout<<"Playing: "<<curr->title<<endl;
		cout<<"Next: "<<n->title<<endl;
	}
	if (curr){
		return curr->title;
	}
}

List list;

void switcher()
{
	cout<<"1. Add music 2. Display music list 3. Edit music 4. Delete music: ";
	char c;
	cin>>c;
	cin.ignore(1,'\n');
	switch (c){
		case '1':{ 
			string mtitle,singer;
			cout<<"Enter music title: "<<endl;
			getline(cin, mtitle);
			cout<<"Enter singer: "<<endl;
			getline(cin, singer);
			list.AddNode(mtitle, singer);
			cout<<"Saved\n";
			switcher();
			break;
		}
		case '2':{
			string ctitle;
			list.PrintList();
			cout<<"Choose a song title: ";
			getline(cin, ctitle);
			int x = list.FindData(ctitle);
			if(x==0){
				cout<<"Does not exist!\n";
				switcher();
			}
			else{
				list.Playing(x);
			}
			switcher();
			break;
		}
		case '3':{
			string edit,newTitle,newSinger;
			list.PrintList();
			cout<<"Enter music title to edit:\n";
			getline(cin,edit);
			int dex = list.FindData(edit)-1;
			cout<<"Enter the new title:\n";
			getline(cin, newTitle);
			cout<<"Enter the new singer:\n";
			getline(cin, newSinger);
			list.EditNode(dex, newTitle, newSinger);
			cout<<"Music Edited\n";
			switcher();
			break;
		}
		case '4':{
			string mtitle;
			list.PrintList();
			cout<<"Enter music title to delete:\n";
			getline(cin, mtitle);
			list.DeleteNode(mtitle);
			cout<<"Deleted\n";
			switcher();
			break;
		}
		default:{
			cout<<"Invalid choice\n";
			switcher();
			break;
		}
	}
}

int main(){
	list.AddNode("Hello", "Adele");
	list.AddNode("7 Rings", "Arianna Grande");
	list.AddNode("Nightmare","Halsey");
	list.AddNode("Sorry","Halsey");
	switcher();
}
