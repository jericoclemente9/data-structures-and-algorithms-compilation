#include <iostream>
#include <windows.h>
#include <string>
#include <sstream>
#include <cstdlib>


using namespace std;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

class List{
	private:
		typedef struct node{
			int counter;
			string title;
			string singer;
			string album;
			string genre;
			string alpha;
			bool isQueued;
			bool isListed;
			int listOrder;
			int queueOrder;
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr tail;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;
	public:
		List();
		void addNode(string addtitle, string addsinger, string addalbum, string addgenre, string alpha);
		void display();
		void displayPlaylist();
		int ToPlaylist(int index, int NumInList);
		int RemoveFromList(int delIndex);
		void playPlaylist(int number);
		bool displayQueue();
		int EnQueue(int index, int NumInList);
		void DeQueue();
		void Sort();
		int FindMusic(string findTitle);
		
};

List::List()
{
	nodePtr head = NULL;
	nodePtr tail = NULL;
	nodePtr curr = NULL;
	nodePtr temp = NULL;
	nodePtr top = NULL;
}
void List::addNode(string addtitle, string addsinger, string addalbum, string addgenre, string alpha)
{
	nodePtr n = new node;
	n->title = addtitle;
	n->singer = addsinger;
	n->album = addalbum;
	n->genre = addgenre;
	n->alpha = alpha;
	n->isQueued = false;
	n->isListed = false;
	n->listOrder = 0;
	n->queueOrder = 0;
	if(head!=NULL){
		curr=head;
		while(curr && curr->next!=NULL){
			curr = curr->next;
		}
		n->counter = curr->counter+1;
		curr->next = n;
		tail = n;
	}
	else{
		n->counter = 1;
		head = n;
		tail = n;
	}
	n->next = NULL;
}

void List::display()
{
	cout<<"*******************************LIBRARY*******************************\n";
	curr = head;
	if (curr!=NULL){
		while (curr && curr->next!=NULL){
			
			cout<<curr->counter<<". "<<"Title: "<<curr->title<<" by: "<<curr->singer<<"; Album: "<<curr->album<<" Genre: "<<curr->genre<<endl;
			curr=curr->next;
		}
	}
	else{
		cout<<"Empty!\n";
	}
	cout<<"*********************************************************************\n";
}

void List::displayPlaylist()
{
	int currIndex = 0;
	while (curr && curr->next!=NULL){
		if (curr->listOrder!=0){
			currIndex++;
		}
		curr = curr->next;
	}
	cout<<"PlayList:\n";
		for (int i = tail->counter; i>=0; i--){
				curr = head;
				while (curr!=NULL && curr->listOrder!=i){
					curr = curr->next;
				}
				if (curr==NULL||curr->listOrder==0||curr->isListed==false){
					
				}
				else{
					cout<<curr->listOrder<<". "<<"Title: "<<curr->title<<" by: "<<curr->singer<<"; Album: "<<curr->album<<" Genre: "<<curr->genre<<endl;
				}
	}
}


int List::ToPlaylist(int index, int NumInList)
{
	curr = head;
	while (curr && curr->counter!=index){
		curr = curr->next;
	}
	if (curr->isListed){
		cout<<"Music is already in the playlist\n";
		return 0;
	}
	else if (curr == NULL || curr->counter == 0){
		cout<<"Music does not exist!\n";
		return 0;
	}
	else {
		curr->isListed = true;
		curr->listOrder = NumInList;
		top = curr;
		return 1;
	}
	
}

int List::RemoveFromList(int delIndex)
{
	int delData = delIndex -1;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> listOrder != delData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->counter ==0)
	{
		return 0;
	}
	else if(curr->isListed==false)
	{
		return 0;
	}
	else
	{	
		cout << "REMOVED"<< endl;
		curr->listOrder = 0;
		curr->isListed =false;
		return 1; 
	}
}

void List::playPlaylist(int number)
{
	int currIndex = 0;
	curr = head;
	while (curr && curr->next!=NULL){
		if (curr->listOrder!=0){
			currIndex++;
		}
		curr = curr->next;
	}
	cout<<"Total in playlist: "<<currIndex<<endl;
	if(currIndex != 0){
		if (number !=0){
				while (number <= currIndex){
					curr = head;
					while(curr->listOrder!=number){
						curr = curr->next;
					}
					cout<<"Playing: "<<curr->title<<endl;
					cout<<"Wanna play the next one? (y/n): ";
					char c;
					cin>>c;
					cin.ignore(1,'\n');
					if (c=='y'||c=='Y'){
						number++;
					}
					else{
						break;
					}
				}
		}
	}
	cout<<"\nThere is nothing more to play!\n";
}

bool List::displayQueue()
{
	cout<<"Queue:\n";
	int queueIndex = 0;
	curr = head;
	while (curr && curr->next!=NULL){
		if (curr->queueOrder !=0){
			queueIndex++;
		}
		curr = curr->next;
	}
	if (queueIndex!=0){
		for (int i = 0; i<= tail->counter; i++){
			curr = head;
			while (curr && curr->queueOrder != i){
				curr = curr->next;
			}
			if (curr==NULL||curr->queueOrder==0||curr->isQueued==false){
			}
			else{
				cout<<curr->queueOrder<<". "<<"Title: "<<curr->title<<" by: "<<curr->singer<<"; Album: "<<curr->album<<" Genre: "<<curr->genre<<endl;
			}
		}
		return true;
	}
	else{
		cout<<"Queue is empty!\n";
		return false;
	}
}

int List::EnQueue(int index, int NumInList)
{
	curr = head;
	while(curr && curr->counter!=index){
		curr = curr->next;
	}
	if (curr->isQueued){
		cout<<"Music is already on queue.\n";
		return 0;
	}
	else if (curr == NULL && curr->counter == 0){
		cout<<"Music does not exist!\n";
		return 0;
	}
	else{
		curr->isQueued = true;
		curr->queueOrder = NumInList;
		return 1;
	}
}

void List::DeQueue()
{
	curr = head;
	int queueIndex = 0, q = 1;
	while(curr && curr->next!=NULL)
	{
		if(curr->queueOrder!=0)
		{
			curr->queueOrder=curr->queueOrder-1;
			if(curr->queueOrder==0)
			{
				curr->isQueued=false;			
			}
		}
		curr=curr->next;
	}
}	

void List::Sort()
{
	
	int i = 1;
	while (i<27){
		stringstream ss;
		ss<<i;
		string eq = ss.str();
		curr = head;
		while (curr->next != NULL){
			if (curr->alpha == eq){
				cout<<"Singer: "<<curr->singer<<" ----- "<<curr->title<<endl;
			}
			curr= curr->next;
		}
		i++;
	}
}

List list;
int listnum=1;
int queuenum = 1;
void switcher()
{
	system("cls");
	SetConsoleTextAttribute(hConsole, 10); 
	cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout<<"||  ##           ############  ############### ###############      ##############    ## ||\n";
	cout<<"||  ##           ############  ############### ###############    ################    ## ||\n";
	cout<<"||  ##           ##                        ###             ###  ###                   ## ||\n";
	cout<<"||  ##           ##                       ###             ###   ###                   ## ||\n";
	cout<<"||  ##           #######                ###             ###     ###      ###########  ## ||\n";
	cout<<"||  ##           #######              ###            ###        ###      ##########   ## ||\n";
	cout<<"||  ##           ##                 ###            ###          ###              ###  ## ||\n";
	cout<<"||  ##           ##              ###             ###            ###              ###  ## ||\n";
	cout<<"||  ##           ##            ###             ###              ###              ###     ||\n";
	cout<<"||  ############ ############  ############### ###############    #################   ## ||\n";
	cout<<"||  ############ ############  ############### ###############      ##############    ## ||\n";
	cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout<<"1) Create Playlist 2) View Playlist 3) View Queue 4) View Library 5) View Sorted 6) Exit";
	cout<<"\nEnter Number: ";
	int choice;
	cin>>choice;
	if(cin.fail()||choice<1||choice>6){
		cin.clear();
		cin.ignore();
		SetConsoleTextAttribute(hConsole, 12); 
		cout<<"Wrong Input!\n";
	}
	else{
		switch (choice){
			case 1:{
				//add to playlist/queue,remove
				system("cls");
				SetConsoleTextAttribute(hConsole, 11); 
				list.display();
				SetConsoleTextAttribute(hConsole, 14); 
				cout<<"1) Add music 2) Remove Music 3) Back: ";
				int c;
				cin>>c;
				if (cin.fail()||c<1||c>3){
					cin.clear();
					cin.ignore();
					SetConsoleTextAttribute(hConsole, 12);
					cout<<"Invalid input!\n";
				}
				else{
					switch(c){
						case 1:{
							SetConsoleTextAttribute(hConsole, 14);
							cout<<"====================================\n";
							cout<<"Enter number you would like to add: ";
							int dex;
							cin>>dex;
							if (cin.fail()||dex<1||dex>1000){
								cin.clear();
								cin.ignore();
								SetConsoleTextAttribute(hConsole, 12);
								cout<<"Error!\n";
							}
							else{
								listnum = list.ToPlaylist(dex,listnum)+listnum;
								cout<<"Music has been added!\n";
							}
							cout<<"====================================\n";
							break;
						}
						case 2:{
							system("cls");
							SetConsoleTextAttribute(hConsole, 11); 
							list.displayPlaylist();
							SetConsoleTextAttribute(hConsole, 14);
							listnum = listnum-list.RemoveFromList(listnum);
							break;
						}
						case 3:{
							switcher();
							break;
						}
					}
				}
				break;
			}
			case 2:{
				SetConsoleTextAttribute(hConsole, 11); 
				list.displayPlaylist();
				SetConsoleTextAttribute(hConsole, 14);
				cout<<"\nDo you want to play? (y/n): ";
				char c;
				cin>>c;
				cin.ignore(1,'\n');
				if (c=='Y'||c=='y'){
					int num=1;
					list.playPlaylist(num);
				}
				break;
			}
			case 3:{
				//display on queue
				SetConsoleTextAttribute(hConsole, 11); 
				list.display();
				SetConsoleTextAttribute(hConsole, 14); 
				cout<<"1) EnQueue a music 2) DeQueue a music 3) View Queue 4) Exit: ";
				int c;
				cin>>c;
				cin.ignore(1,'\n');
				if (cin.fail()){
					cin.clear();
					cin.ignore();
					SetConsoleTextAttribute(hConsole, 12);
					cout<<"\nInvalid input!";
				}
				else{
					SetConsoleTextAttribute(hConsole, 14); 
					switch (c){
						case 1:{
							cout<<"\nEnter number to be queued: ";
							int index;
							cin>>index;
							cin.ignore(1,'\n');
							if(index<1||index>1000){
								SetConsoleTextAttribute(hConsole, 12); 
								cout<<"Invalid input!\n";
							}
							else{
								queuenum = list.EnQueue(index, queuenum)+queuenum;
								cout<<"Music has been queued!\n";
							}
							break;
						}
						case 2:{
							SetConsoleTextAttribute(hConsole, 14); 
							bool x = list.displayQueue();
							if (x){
								cout<<"The first item has been removed\n";
								list.DeQueue();
							}
							else{
								cout<<"Nothing to be removed!\n";
							}
							break;
						}
						case 3:{
							SetConsoleTextAttribute(hConsole, 14);
							list.displayQueue();
							break;
						}
						case 4:{
							//exit
							switcher();
							break;
						}
					}
				}
				break;
			}
			case 4:{
				//display whole library
				SetConsoleTextAttribute(hConsole, 11); 
				list.display();
				break;
			}
			case 5:{
				//sort playlist/queue
				SetConsoleTextAttribute(hConsole, 14); 
				list.Sort();
				break;
			}
			case 6:{
				//exit
				exit(0);
				break;
			}
			default:{
				cout<<"Invalid choice!\n";
				break;
			}
		}
	}
	system("PAUSE");
	switcher();
}

int main()     
{
	string arr[1001][5] = {
	{"Intrapersonal","Turnover","Peripheral Vision","Alternative","20"},
	{"Would Hate You If I Could","Turnover","Peripheral Vision","Alternative","20"},
	{"Like Slowly Disappearing","Turnover","Peripheral Vision","Alternative","20"},
	{"Humming","Turnover","Peripheral Vision","Alternative","20"},
	{"Threshold","Turnover","Peripheral Vision","Alternative","20"},
	{"New Scream","Turnover","Peripheral Vision","Alternative","20"},
	{"Diazepam","Turnover","Peripheral Vision","Alternative","20"},
	{"Take My Head","Turnover","Peripheral Vision","Alternative","20"},
	{"Cutting My Fingers Off","Turnover","Peripheral Vision","Alternative","20"},
	{"Dizzy On The Comedown","Turnover","Peripheral Vision","Alternative","20"},
	{"Intro","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"January 28th","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Wet Dreamz","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"03' Adolescence","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"A Tale of 2 Citiez","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Fire Squad","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"St. Tropez","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"G.O.M.D","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"No Role Modelz","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Hello","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Apparently","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Love Yourz","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Note To Self","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Citezens Of Earth","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"I Hope This Comes Back To Haunt You","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"December","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Smooth Seas Don’t Make Good Sailors","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Cant Kick Up The Roots","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Gold Steps","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Kali Ma","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Rock Bottom","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"The Beach is For Lovers Not Losers","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Serpents","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Grenade","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Just the Way you Are","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Our First Time","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Runaway Baby","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"The Lazy Song","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Marry You","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Talking to the Moon","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Liquor Store Blues","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Count on Me","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"The Other Side","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Somewhere in Brooklyn","Bruno Mars","Doo-Wops & Hooligans","Pop","2"},
	{"Superproxy","Eraserheads","Cutterpillow","Pop","5"},
	{"Back2me","Eraserheads","Cutterpillow","Pop","5"},
	{"Waiting for the Bus","Eraserheads","Cutterpillow","Pop","5"},
	{"Fine Time","Eraserheads","Cutterpillow","Pop","5"},
	{"Kama Supra","Eraserheads","Cutterpillow","Pop","5"},
	{"Overdrive","Eraserheads","Cutterpillow","Pop","5"},
	{"Slo Mo","Eraserheads","Cutterpillow","Pop","5"},
	{"Torpedo","Eraserheads","Cutterpillow","Pop","5"},
	{"Huwag mo nang Itanong","Eraserheads","Cutterpillow","Pop","5"},
	{"Paru-Parong Ningning","Eraserheads","Cutterpillow","Pop","5"},
	{"Walang Nagbago","Eraserheads","Cutterpillow","Pop","5"},
	{"Cutterpillow","Eraserheads","Cutterpillow","Pop","5"},
	{"Poorman's Grave","Eraserheads","Cutterpillow","Pop","5"},
	{"The Phoenix","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"My Songs Know What You Did in the Dark (Light Em Up)","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Alone Together","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Where Did the Party Go","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Just One Yesterday","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"The Mighty Fall","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Miss Missing You","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Death Valley","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Young Volcanoes","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Rat a Tat","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Save Rock and Roll","Fallout Boy","Save Rock and Roll","Pop-Punk","6"},
	{"Hourglass","Zedd","Clarity","Dance/Electronic","26"},
	{"Shave it","Zedd","Clarity","Dance/Electronic","26"},
	{"Spectrum","Zedd","Clarity","Dance/Electronic","26"},
	{"Lost at Sea","Zedd","Clarity","Dance/Electronic","26"},
	{"Clarity","Zedd","Clarity","Dance/Electronic","26"},
	{"Codec","Zedd","Clarity","Dance/Electronic","26"},
	{"Stache","Zedd","Clarity","Dance/Electronic","26"},
	{"Fall in to Sky","Zedd","Clarity","Dance/Electronic","26"},
	{"Follow you Down","Zedd","Clarity","Dance/Electronic","26"},
	{"Epos","Zedd","Clarity","Dance/Electronic","26"},
	{"Stay The Night","Zedd","Clarity","Dance/Electronic","26"},
	{"Push Play","Zedd","Clarity","Dance/Electronic","26"},
	{"Alive","Zedd","Clarity","Dance/Electronic","26"},
	{"Breakin a Sweat","Zedd","Clarity","Dance/Electronic","26"},
	{"Weightless","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Break Your Little Heart","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Damned If I Do Ya, Damned If I Don't","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Lost In Stereo","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Stella","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Sick Little Games","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Hello, Brooklyn","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Walls","All Time Low","Nothing Personal","Pop-Punk","1"},
	//100
	{"Too Much","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Keep The Change, You Filthy Animal","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"A Party Song (The Walk Of Shame)","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"Therapy","All Time Low","Nothing Personal","Pop-Punk","1"},
	{"All We Know","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Pressure","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Emergency","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Brighter","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Here We Go Again","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Let This Go","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Woah","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Conspiracy","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Franklin","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"My Heart","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Scene One - James Dean & Audrey Hepburn","Sleeping With Sirens","If You Were A Movie,...","Acoustic","19"},
	{"Scene Two - Roger Rabbit","Sleeping With Sirens","If You Were A Movie,...","Acoustic","19"},
	{"Scene Three - Stomach Tied In Knots","Sleeping With Sirens","If You Were A Movie,...","Acoustic","19"},
	{"Scene Four - Don't You Ever Forget About Me","Sleeping With Sirens","If You Were A Movie,...","Acoustic","19"},
	{"Scene Five - With Ears To See and Eyes To Hear","Sleeping With Sirens","If You Were A Movie,...","Acoustic","19"},
	{"Warm me up","The Audition","Champion","Pop-Punk","20"},
	{"Make It Rain","The Audition","Champion","Pop-Punk","20"},
	{"Shady Business","The Audition","Champion","Pop-Punk","20"},
	{"Nothing","The Script","Science & Faith","Alternative Rock","20"},
	{"If you ever come back","The Script","Science & Faith","Alternative Rock","20"},
	{"Science & Faith","The Script","Science & Faith","Alternative Rock","20"},
	{"Long Gone and Moved on","The Script","Science & Faith","Alternative Rock","20"},
	{"Deadman Walking","The Script","Science & Faith","Alternative Rock","20"},
	{"This Love","The Script","Science & Faith","Alternative Rock","20"},
	{"Walk Away","The Script","Science & Faith","Alternative Rock","20"},
	{"Exit Wounds","The Script","Science & Faith","Alternative Rock","20"},
	{"You won't feel a Thing","The Script","Science & Faith","Alternative Rock","20"},
	{"Dirty Little Secret","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Stab My Back","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Move Along","The All-American Reject","Move Along","Pop-Punk","20"},
	{"It Ends Tonight","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Change Your Mind","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Night Drive","The All-American Reject","Move Along","Pop-Punk","20"},
	{"11:11 PM","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Dance Inside","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Top of the World","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Straitjacket Feeling ","The All-American Reject","Move Along","Pop-Punk","20"},
	{"I'm waiting","The All-American Reject","Move Along","Pop-Punk","20"},
	{"Can't take it","The All-American Reject","Move Along","Pop-Punk","20"},
	{"God Is A Woman","Ariana Grande","Sweetener","Pop","1"},
	{"Sweetener","Ariana Grande","Sweetener","Pop","1"},
	{"Breathin","Ariana Grande","Sweetener","Pop","1"},
	{"No Tears Left To Cry","Ariana Grande","Sweetener","Pop","1"},
	{"Everytime","Ariana Grande","Sweetener","Pop","1"},
	{"Get Well Soon","Ariana Grande","Sweetener","Pop","1"},
	{"Goodnight n Go","Ariana Grande","Sweetener","Pop","1"},
	{"R.E.M.","Ariana Grande","Sweetener","Pop","1"},
	{"The Light is Coming","Ariana Grande","Sweetener","Pop","1"},
	{"State of Grace","Taylor Swift","Red","Pop","20"},
	{"Red","Taylor Swift","Red","Pop","20"},
	{"I Knew You Were Trouble","Taylor Swift","Red","Pop","20"},
	{"All Too Well","Taylor Swift","Red","Pop","20"},
	{"22","Taylor Swift","Red","Pop","20"},
	{"I Almost Do","Taylor Swift","Red","Pop","20"},
	{"We Are Never Ever Getting Back Together","Taylor Swift","Red","Pop","20"},
	{"Starlight","Taylor Swift","Red","Pop","20"},
	{"Holy Ground","Taylor Swift","Red","Pop","20"},
	{"Begin Again","Taylor Swift","Red","Pop","20"},
	{"Eyes Closed","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Alone","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Now or Never","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Sorry","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Bad At Love","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Devil In Me","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Assertion of the Heart","HoneyWorks","HoneyWorks","Rock","8"},
	{"Bae Love","HoneyWorks","HoneyWorks","Rock","8"},
	{"Bloom in Love Color","HoneyWorks","HoneyWorks","Rock","8"},
	{"Brazen Honey","HoneyWorks","HoneyWorks","Rock","8"},
	{"Can I confess to you?","HoneyWorks","HoneyWorks","Rock","8"},
	{"The day I knew Love","HoneyWorks","HoneyWorks","Rock","8"},
	{"Declaration of the Weak","HoneyWorks","HoneyWorks","Rock","8"},
	{"Dream Fanfare","HoneyWorks","HoneyWorks","Rock","8"},
	{"Fansa","HoneyWorks","HoneyWorks","Rock","8"},
	{"I like you now","HoneyWorks","HoneyWorks","Rock","8"},
	{"Inokori-sensei","HoneyWorks","HoneyWorks","Rock","8"},
	{"Light Proof Theory","HoneyWorks","HoneyWorks","Rock","8"},
	{"Love meets and love continues","HoneyWorks","HoneyWorks","Rock","8"},
	{"Maidens","HoneyWorks","HoneyWorks","Rock","8"},
	{"Mama","HoneyWorks","HoneyWorks","Rock","8"},
	{"Monday's Melancholy","HoneyWorks","HoneyWorks","Rock","8"},
	{"Mr. Darling","HoneyWorks","HoneyWorks","Rock","8"},
	{"Nostalgic Rainfall","HoneyWorks","HoneyWorks","Rock","8"},
	{"Picture Book of my first love","HoneyWorks","HoneyWorks","Rock","8"},
	{"Secret of Sunday","HoneyWorks","HoneyWorks","Rock","8"},
	{"Sick name love wazurai","HoneyWorks","HoneyWorks","Rock","8"},
	{"A small lion","HoneyWorks","HoneyWorks","Rock","8"},
	//200
	{"Tokyo summer session","HoneyWorks","HoneyWorks","Rock","8"},
	{"Twins","HoneyWorks","HoneyWorks","Rock","8"},
	{"PILLOWTALK","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"iT’s YoU","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"BeFoUr","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"sHe","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"dRuNk","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"rEaR vIeW","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"wRoNg","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"fOoL fOr YoU","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"BoRdErZ","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"tRuTh","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"lUcOzAdE","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"TiO","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"BLUE","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"BRIGHT","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"LIKE I WOULD","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"SHE DON’T LOVE ME","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"Steal My Girl","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Ready to Run","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Where Do Broken Hearts Go","One Direction","FOUR","Pop music, Pop rock","15"},
	{"18","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Girl Almighty","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Fool’s Gold","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Night Changes","One Direction","FOUR","Pop music, Pop rock","15"},
	{"No Control","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Fireproof","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Spaces","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Stockholm Syndrome","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Clouds","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Change Your Ticket","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Illusion","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Once in a Lifetime","One Direction","FOUR","Pop music, Pop rock","15"},
	{"Act My Age","One Direction","FOUR","Pop music, Pop rock","15"},
	{"One","Ed Sheeran","Divide","Pop","5"},
	{"I'm a mess","Ed Sheeran","Divide","Pop","5"},
	{"Sing","Ed Sheeran","Divide","Pop","5"},
	{"Don't","Ed Sheeran","Divide","Pop","5"},
	{"Nina","Ed Sheeran","Divide","Pop","5"},
	{"Photograph","Ed Sheeran","Divide","Pop","5"},
	{"Bloodstream","Ed Sheeran","Divide","Pop","5"},
	{"Terenife Sea","Ed Sheeran","Divide","Pop","5"},
	{"Thinking out loud","Ed Sheeran","Divide","Pop","5"},
	{"Eraser","Ed Sheeran","Divide","Pop","5"},
	{"Castle on the hill","Ed Sheeran","Divide","Pop","5"},
	{"Dive","Ed Sheeran","Divide","Pop","5"},
	{"Shape of you","Ed Sheeran","Divide","Pop","5"},
	{"Perfect","Ed Sheeran","Divide","Pop","5"},
	{"Galway Girl","Ed Sheeran","Divide","Pop","5"},
	{"Happier","Ed Sheeran","Divide","Pop","5"},
	{"New man","Ed Sheeran","Divide","Pop","5"},
	{"Hearts don't break around here","Ed Sheeran","Divide","Pop","5"},
	{"What do i know","Ed Sheeran","Divide","Pop","5"},
	{"How do you feel","Ed Sheeran","Divide","Pop","5"},
	{"Supermarket Flowers","Ed Sheeran","Divide","Pop","5"},
	{"Barcelona","Ed Sheeran","Divide","Pop","5"},
	{"Bibia be ye ye","Ed Sheeran","Divide","Pop","5"},
	{"Nancy Mulligan","Ed Sheeran","Divide","Pop","5"},
	{"Save yourself","Ed Sheeran","Divide","Pop","5"},
	{"Welcome To New York","Taylor Swift","1989","Pop","20"},
	{"Blank Space","Taylor Swift","1989","Pop","20"},
	{"Style","Taylor Swift","1989","Pop","20"},
	{"Out Of The Woods","Taylor Swift","1989","Pop","20"},
	{"All you had to do was stay","Taylor Swift","1989","Pop","20"},
	{"Shake it Off","Taylor Swift","1989","Pop","20"},
	{"I wish you would","Taylor Swift","1989","Pop","20"},
	{"Bad Blood","Taylor Swift","1989","Pop","20"},
	{"Wildest Dreams","Taylor Swift","1989","Pop","20"},
	{"How You Get The Girl","Taylor Swift","1989","Pop","20"},
	{"This Love","Taylor Swift","1989","Pop","20"},
	{"I know Places","Taylor Swift","1989","Pop","20"},
	{"Clean","Taylor Swift","1989","Pop","20"},
	{"Hard Times","Paramore","After Laughter","Alternative/Indie","16"},
	{"Rose- Colored Boy","Paramore","After Laughter","Alternative/Indie","16"},
	{"Told You So","Paramore","After Laughter","Alternative/Indie","16"},
	{"Forgiveness","Paramore","After Laughter","Alternative/Indie","16"},
	{"Fake Happy","Paramore","After Laughter","New wave, pop rock","16"},
	{"26","Paramore","After Laughter","Alternative/Indie","16"},
	{"Pool","Paramore","After Laughter","Alternative/Indie","16"},
	{"Grudges","Paramore","After Laughter","Alternative/Indie","16"},
	{"Caught In the Middle","Paramore","After Laughter","Alternative/Indie","16"},
	{"Idle Worship","Paramore","After Laughter","Alternative/Indie","16"},
	{"No Friend ","Paramore","After Laughter","Alternative/Indie","16"},
	{"Tell Me How","Paramore","After Laughter","Alternative/Indie","16"},
	{"Need You Now","Lady Antebellum ","Need You Now ","Country","12"},
	{"Our Kind Of Love","Lady Antebellum ","Need You Now ","Country","12"},
	{"American Honey","Lady Antebellum ","Need You Now ","Country","12"},
	{"Hello World","Lady Antebellum ","Need You Now ","Country","12"},
	{"Perfect Day","Lady Antebellum ","Need You Now ","Country","12"},
	{"Love This Pain","Lady Antebellum ","Need You Now ","Country","12"},
	//300
	{"When You Got A Good Thing","Lady Antebellum ","Need You Now ","Country","12"},
	{"Stars Tonight ","Lady Antebellum ","Need You Now ","Country","12"},
	{"If I Know Then","Lady Antebellum ","Need You Now ","Country","12"},
	{"Something Bout A Woman","Lady Antebellum ","Need You Now ","Country","12"},
	{"Home","Lady Antebellum ","HeartBreak","Country","12"},
	{"Heart Break","Lady Antebellum ","HeartBreak","Country","12"},
	{"You Look Good","Lady Antebellum ","HeartBreak","Country","12"},
	{"Somebody's Else's Heart","Lady Antebellum ","HeartBreak","Country","12"},
	{"This City","Lady Antebellum ","HeartBreak","Country","12"},
	{"Hurt","Lady Antebellum ","HeartBreak","Country","12"},
	{"Army","Lady Antebellum ","HeartBreak","Country","12"},
	{"Good Time To Be Alive","Lady Antebellum ","HeartBreak","Country","12"},
	{"Think About You","Lady Antebellum ","HeartBreak","Country","12"},
	{"Big Love In A Small Town","Lady Antebellum ","HeartBreak","Country","12"},
	{"Angel With A Shotgun","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Endlessly ","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Animal","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Intoxicated ","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Lala","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Her Love Is My Religion ","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Another Me","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Lovesick Fool","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"I'd do Anything","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"The Worst Day Ever","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"You Don't Mean Anything","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"I'm Just A Kid","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"When I'm With You","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Meet You There","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Addicted","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"My Alien","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"God Must Hate Me","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"I Won't Be There","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"One Day","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Perfect","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Grow Up","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Cry Baby","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Dollhouse","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Sippy Cup","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Carousel","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Alphabet Boy","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Soap","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Training Wheels","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Pity Party","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Tag, You're It.","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Milk and Cookies","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Pacify Her","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Mrs. Potato Head","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Mad Hatter","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"A Head Full Of Dreams","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Birds","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Hymn for the Weekend","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Everglow","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Adventure Of A Lifetime","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Fun","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Kaleidoscope","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Army of One","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Colour Spectrum","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Amazing Day","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Up&Up","Coldplay","A Head Full of Dreams","Pop Music, Pop Rock","3"},
	{"Dumb Stuff","Lany","Lany","Electropop","12"},
	{"The Breakup","Lany","Lany","Electropop","12"},
	{"Super Far","Lany","Lany","Electropop","12"},
	{"13","Lany","Lany","Electropop","12"},
	{"Overtime","Lany","Lany","Electropop","12"},
	{"Flowers on the Floor","Lany","Lany","Electropop","12"},
	{"Parents","Lany","Lany","Electropop","12"},
	{"ILYSB","Lany","Lany","Electropop","12"},
	{"Hericane","Lany","Lany","Electropop","12"},
	{"Hurts","Lany","Lany","Electropop","12"},
	{"Good Girls","Lany","Lany","Electropop","12"},
	{"Pancakes","Lany","Lany","Electropop","12"},
	{"Tampa","Lany","Lany","Electropop","12"},
	{"Purple Teeth","Lany","Lany","Electropop","12"},
	{"It Was Love","Lany","Lany","Electropop","12"},
	{"So, Soo Pretty","Lany","Lany","Electropop","12"},
	{"Helena","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Give 'Em Hell Kid","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"To The End","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"You Know What They Do With Guys Like Us In Prison","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"I'm Not Okay (I Promise)","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"The Ghost of You","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"The Jetset Life is Gonna Kill You","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Interlude","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Thank You For The Venom","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Hang 'Em High","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"It's Not a Fashion Statement, It's a Deathwish","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Cemetery Drive","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"I Never Told You What I Do for a Living","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Radioactive","Imagine Dragons","Night Visions","Alternative","9"},
	{"Tiptoe","Imagine Dragons","Night Visions","Alternative","9"},
	//400
	{"It's Time","Imagine Dragons","Night Visions","Alternative","9"},
	{"Demons","Imagine Dragons","Night Visions","Alternative","9"},
	{"On Top of the World","Imagine Dragons","Night Visions","Alternative","9"},
	{"Amsterdam","Imagine Dragons","Night Visions","Alternative","9"},
	{"Hear Me","Imagine Dragons","Night Visions","Alternative","9"},
	{"Every Night","Imagine Dragons","Night Visions","Alternative","9"},
	{"Bleeding Out","Imagine Dragons","Night Visions","Alternative","9"},
	{"Underdog","Imagine Dragons","Night Visions","Alternative","9"},
	{"Nothing Left to Say","Imagine Dragons","Night Visions","Alternative","9"},
	{"Cha-Ching (Till We Grow Older)","Imagine Dragons","Night Visions","Alternative","9"},
	{"Working Man","Imagine Dragons","Night Visions","Alternative","9"},
	{"Tokyo","Imagine Dragons","Night Visions","Alternative","9"},
	{"Jumpsuit","Twenty One Pilots","Trench","Pop","20"},
	{"Levitate","Twenty One Pilots","Trench","Pop","20"},
	{"Morph","Twenty One Pilots","Trench","Pop","20"},
	{"My Blood","Twenty One Pilots","Trench","Pop","20"},
	{"Chlorine","Twenty One Pilots","Trench","Pop","20"},
	{"Smithereens","Twenty One Pilots","Trench","Pop","20"},
	{"Neon Gravestones","Twenty One Pilots","Trench","Pop","20"},
	{"The Hype","Twenty One Pilots","Trench","Pop","20"},
	{"Nico and the Niners","Twenty One Pilots","Trench","Pop","20"},
	{"Cut My Lip","Twenty One Pilots","Trench","Pop","20"},
	{"Bandito","Twenty One Pilots","Trench","Pop","20"},
	{"Pet Cheetah","Twenty One Pilots","Trench","Pop","20"},
	{"Legend","Twenty One Pilots","Trench","Pop","20"},
	{"Leave the City","Twenty One Pilots","Trench","Pop","20"},
	{"Para Sayo","Parokya Ni Edgar","Inuman Session Vol.2","Rock","16"},
	{"Gitara","Parokya Ni Edgar","Inuman Session Vol.2","Rock","16"},
	{"Akala","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Pakiusap Lang (Lasingin Nyo Ako)","Parokya Ni Edgar","Inuman Session Vol.2","Rock","16"},
	{"Pangarap Lang Kita","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"The Ordertaker (ft. Kamikazee)","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"One Hit Combo","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Bagsakan (ft. Francis M. & Glock-9","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Alumni Homecoming ","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Your Song ","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Wala Lang Yun","Parokya Ni Edgar","Pogi Years Old","Pop","16"},
	{"Lagi Mong Tatandaan","Parokya Ni Edgar","Pogi Years Old","Pop","16"},
	{"Panahon Na Naman Ng Harana ","Parokya Ni Edgar","Pogi Years Old","Pop","16"},
	{"Buloy ","Parokya Ni Edgar","Bente","Rock","16"},
	{"Maniwala Ka Sana","Parokya Ni Edgar","Bente","Rock","16"},
	{"Silvertoes","Parokya Ni Edgar","Bente","Pop","16"},
	{"Harana","Parokya Ni Edgar","Bente","Pop","16"},
	{"Sorry Na","Parokya Ni Edgar","Bente","Pop","16"},
	{"Ok Lang Ako","Parokya Ni Edgar","Bigotilyo","Rock","16"},
	{"Choco Latte","Parokya Ni Edgar","Bigotilyo","Pop","16"},
	{"Parang Ayoko Na Yata","Parokya Ni Edgar","Bigotilyo","Pop","16"},
	{"Pasensya Ka Na","Silent Sanctuary","Langit, Luha","Pop","19"},
	{"Rebound ","Silent Sanctuary","Fuchsiang Pag-ibig","Pop","19"},
	{"Kundiman","Silent Sanctuary","Fuchsiang Pag-ibig","Pop","19"},
	{"Sandali Lang","Silent Sanctuary","Fuchsiang Pag-ibig","Pop","19"},
	{"4 Your Eyez Only","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Change","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Deja Vu","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Foldin Clothes","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"For Whom The Bells Toll","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Immortal","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Neighbors","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"She's Mine Pt. 1","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"She's Mine Pt. 2","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Ville Mentality","J Cole","4 Your Eyez Only","Hip-Hop","10"},
	{"Casual Affair","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Collar Full ","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Far Too Young To Die","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Girl That You Love","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"GirlsGirlsBoys","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Miss Jackson","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Nicotine","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"The End Of All Things","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"This Is Gospel","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Vegas Lights","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Battery ","Metallica","Master Of Puppets","Metal","13"},
	{"Damage, Inc.","Metallica","Master Of Puppets","Metal","13"},
	{"Disposable Heroes","Metallica","Master Of Puppets","Metal","13"},
	{"Leper Messiah","Metallica","Master Of Puppets","Metal","13"},
	{"Master Of Puppets","Metallica","Master Of Puppets","Metal","13"},
	{"Orion","Metallica","Master Of Puppets","Metal","13"},
	{"The Thing That Should Be","Metallica","Master Of Puppets","Metal","13"},
	{"Welcome Home (Sanitarium)","Metallica","Master Of Puppets","Metal","13"},
	{"City Lights","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Sleep Tonight","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Fallin’","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Ears and Rhymes","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"I’ll Be Watching You","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Back To Love","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Eroplanong Papel","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Dive","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Forever","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Breathe Again","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Time To Go","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	//500
	{"For the Stunt","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Aint NobodyTakin My Baby","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"2 A.M.","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Down For You","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Off The Strength","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"We Just Havent Met Yet","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Always Knew","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Whenever","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Waste My Time","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Inbetween","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Lost","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Yung God","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Lately","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Comin' Thru","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Connected","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Keep the Faith","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"All My Angels","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Brush Me","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"24K Magic","Bruno Mars","24k Magic","Pop","2"},
	{"Chunky","Bruno Mars","24k Magic","Pop","2"},
	{"Perm","Bruno Mars","24k Magic","Pop","2"},
	{"That’s What I Like","Bruno Mars","24k Magic","Pop","2"},
	{"Versace On The Floor","Bruno Mars","24k Magic","Pop","2"},
	{"Straight Up & Down","Bruno Mars","24k Magic","Pop","2"},
	{"Calling All My Lovelies","Bruno Mars","24k Magic","Pop","2"},
	{"Finesse","Bruno Mars","24k Magic","Pop","2"},
	{"Too Good To Say Goodbye","Bruno Mars","24k Magic","Pop","2"},
	{"Limasawa Street","Ben&Ben","Limasawa Street","Pop","2"},
	{"Pagtingin","Ben&Ben","Limasawa Street","Pop","2"},
	{"Fall","Ben&Ben","Limasawa Street","Pop","2"},
	{"Talaarawan","Ben&Ben","Limasawa Street","Pop","2"},
	{"Hummingbird","Ben&Ben","Limasawa Street","Pop","2"},
	{"Araw-Araw","Ben&Ben","Limasawa Street","Pop","2"},
	{"Baka Sakali","Ben&Ben","Limasawa Street","Pop","2"},
	{"Lucena","Ben&Ben","Limasawa Street","Pop","2"},
	{"Sampaguita","Ben&Ben","Limasawa Street","Pop","2"},
	{"War","Ben&Ben","Limasawa Street","Pop","2"},
	{"Godsent","Ben&Ben","Limasawa Street","Pop","2"},
	{"Roots","Ben&Ben","Limasawa Street","Pop","2"},
	{"Clapclapclap!","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Sweet Shadow","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Bata, Dahan-Dahan!","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Bawat Kaluluwa","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Not My Energy","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Come Inside Of My Heart","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"The Novel Of My Mind","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Dulo Ng Hangganan","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"In My Prison","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"My Juliana","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"I'm A Butterfly","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"I Ain't Perfect","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Take That Man","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"I Would Rather Live Alone","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"You Suck at Love","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Can't Keep My Hands off You","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Jet Lag","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Astronaut","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Loser of the Year","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Anywhere Else But Here","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Freaking Me Out","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Summer Paradise","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Gone Too Soon","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Last One Standing","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"This Song Saved My Life","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Shut Up!","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Welcome to My Life","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Perfect World","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Thank You","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Me Against the World","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Jump","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Everytime","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Promise","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"One","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Untitled","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Perfect","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Ready for it?","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Endgame","Taylor Swift","Reputation","Pop-Rock","20"},
	{"I did something bad","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Don't blame me","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Delicate","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Look what you made me do","Taylor Swift","Reputation","Pop-Rock","20"},
	{"So it goes","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Gorgeous","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Getaway car","Taylor Swift","Reputation","Pop-Rock","20"},
	{"King of my heart","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Dress","Taylor Swift","Reputation","Pop-Rock","20"},
	{"This is why we can't have nice things","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Call it what you want","Taylor Swift","Reputation","Pop-Rock","20"},
	{"New Year's Day","Taylor Swift","Reputation","Pop-Rock","20"},
	{"I Forgot that you Existed","Taylor Swift","Lover","Electropop","20"},
	//600
	{"Cruel Summer","Taylor Swift","Lover","Electropop","20"},
	{"Lover","Taylor Swift","Lover","Electropop","20"},
	{"The Man","Taylor Swift","Lover","Electropop","20"},
	{"The Archer","Taylor Swift","Lover","Electropop","20"},
	{"I Think he Knows","Taylor Swift","Lover","Electropop","20"},
	{"Miss Americana & The Heartbreak Prince","Taylor Swift","Lover","Electropop","20"},
	{"Paper Rings","Taylor Swift","Lover","Electropop","20"},
	{"Cornelia Street","Taylor Swift","Lover","Electropop","20"},
	{"Death by a Thousand Cuts","Taylor Swift","Lover","Electropop","20"},
	{"London Boy","Taylor Swift","Lover","Electropop","20"},
	{"Soon You'll get Better","Taylor Swift","Lover","Electropop","20"},
	{"False God ","Taylor Swift","Lover","Electropop","20"},
	{"You Need to Clam Down","Taylor Swift","Lover","Electropop","20"},
	{"Afterglow","Taylor Swift","Lover","Electropop","20"},
	{"Me","Taylor Swift","Lover","Electropop","20"},
	{"It's Nice to Have a Friend","Taylor Swift","Lover","Electropop","20"},
	{"Daylight","Taylor Swift","Lover","Electropop","20"},//584
	{"Dreams","Bazzi","Cosmic","Pop","2"},
	{"Soarin","Bazzi","Cosmic","Pop","2"},
	{"Myself","Bazzi","Cosmic","Pop","2"},
	{"Star","Bazzi","Cosmic","Pop","2"},
	{"Why","Bazzi","Cosmic","Pop","2"},
	{"3:15","Bazzi","Cosmic","Pop","2"},
	{"Honest","Bazzi","Cosmic","Pop","2"},
	{"Mirror","Bazzi","Cosmic","Pop","2"},
	{"Gone","Bazzi","Cosmic","Pop","2"},
	{"Fantasy","Bazzi","Cosmic","Pop","2"},
	{"BRB","Bazzi","Cosmic","Pop","2"},
	{"Cartier","Bazzi","Cosmic","Pop","2"},
	{"Beautiful ","Bazzi","Cosmic","Pop","2"},
	{"Mine","Bazzi","Cosmic","Pop","2"},
	{"Changed","Bazzi","Cosmic","Pop","2"},
	{"Somebody","Bazzi","Cosmic","Pop","2"},
	{"There's Nothing Holdin' Back","Shawn Mendes","Illuminate","Pop","19"},
	{"Ruin","Shawn Mendes","Illuminate","Pop","19"},
	{"Mercy","Shawn Mendes","Illuminate","Pop","19"},
	{"Treat You Better","Shawn Mendes","Illuminate","Pop","19"},
	{"Three Empty Words","Shawn Mendes","Illuminate","Pop","19"},
	{"Don't Be A Fool","Shawn Mendes","Illuminate","Pop","19"},
	{"Like This ","Shawn Mendes","Illuminate","Pop","19"},
	{"No Promises","Shawn Mendes","Illuminate","Pop","19"},
	{"Lights On","Shawn Mendes","Illuminate","Pop","19"},
	{"Honest ","Shawn Mendes","Illuminate","Pop","19"},
	{"Patience","Shawn Mendes","Illuminate","Pop","19"},
	{"Bad Reputation","Shawn Mendes","Illuminate","Pop","19"},
	{"Understand","Shawn Mendes","Illuminate","Pop","19"},
	{"Wallflower","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Radar (ft. HONNE)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Top Shelf (feat. Bipolar Sunshine)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Good Nights (feat. MASCOLO)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Together","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Superlove (feat. Oh Wonder)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Be Like You (feat. Broods)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"I Miss You","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Wilderness","Stephen Speaks","No More Doubt","Pop","19"},
	{"Weather","Stephen Speaks","No More Doubt","Pop","19"},
	{"What'll She Look Like","Stephen Speaks","No More Doubt","Pop","19"},
	{"Filthy","Stephen Speaks","No More Doubt","Pop","19"},
	{"Out of My League","Stephen Speaks","No More Doubt","Pop","19"},
	{"Leaving Song","Stephen Speaks","No More Doubt","Pop","19"},
	{"Feet Wet","Stephen Speaks","No More Doubt","Pop","19"},
	{"Passenger Seat","Stephen Speaks","No More Doubt","Pop","19"},
	{"Communion","Stephen Speaks","No More Doubt","Pop","19"},
	{"Toe The Line","Stephen Speaks","No More Doubt","Pop","19"},
	{"Complete","Stephen Speaks","No More Doubt","Pop","19"},
	{"On My Way","Stephen Speaks","No More Doubt","Pop","19"},
	{"Warriors","Riot Games","Worlds Music","Dance/Electronic"},
	{"Worlds Collide","Riot Games","Worlds Music","Dance/Electronic"},
	{"Ignite","Riot Games","Worlds Music","Dance/Electronic"},
	{"Legends Never die","Riot Games","Worlds Music","Dance/Electronic"},
	{"Rise","Riot Games","Worlds Music","Dance/Electronic"},
	{"Phoenix","Riot Games","Worlds Music","Dance/Electronic"},
	{"Let it Rock","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"You Give Love a Bad Name","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Livin on a Prayer","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Social Disease","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Wanted Dead or Alive","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Raise your Hands","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Without Love","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"I'd Die For You","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Never say Goodbye","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Wild in the Streets","Bon Jovi","Slippery when wet","Pop-Rock","2"},
	{"Mustapha","Queen","Jazz(Queen Album)","Rock","17"},
	{"Fat Bottomed Girls","Queen","Jazz(Queen Album)","Rock","17"},
	{"Jealousy","Queen","Jazz(Queen Album)","Rock","17"},
	{"Bicycle Race","Queen","Jazz(Queen Album)","Rock","17"},
	{"If You Can't Beat Them","Queen","Jazz(Queen Album)","Rock","17"},
	{"Let Me Entertain You","Queen","Jazz(Queen Album)","Rock","17"},
	{"Dead on Time","Queen","Jazz(Queen Album)","Rock","17"},
	{"In Only seven Days","Queen","Jazz(Queen Album)","Rock","17"},
	//700
	{"Dreamer's Balls","Queen","Jazz(Queen Album)","Rock","17"},
	{"Fun It","Queen","Jazz(Queen Album)","Rock","17"},
	{"Leaving Home Aint Easy","Queen","Jazz(Queen Album)","Rock","17"},
	{"Dont Stop Me Now","Queen","Jazz(Queen Album)","Rock","17"},
	{"More of that Jazz","Queen","Jazz(Queen Album)","Rock","17"},
	{"The Haunting","Set it Off","Duality","Punk","19"},
	{"N.M.E.","Set it Off","Duality","Punk","19"},
	{"Forever Stuck in Our Youth","Set it Off","Duality","Punk","19"},
	{"Why Worry","Set it Off","Duality","Punk","19"},
	{"Ancient History","Set it Off","Duality","Punk","19"},
	{"Bleak December","Set it Off","Duality","Punk","19"},
	{"Duality","Set it Off","Duality","Punk","19"},
	{"Wolf in Sheep's Clothing","Set it Off","Duality","Punk","19"},
	{"Tomorrow","Set it Off","Duality","Punk","19"},
	{"Bad Guy","Set it Off","Duality","Punk","19"},
	{"Miss Mysterious","Set it Off","Duality","Punk","19"},
	{"Killer in the Mirror","Set it Off","Midnight","Alternative/Indie","19"},
	{"Hourglass","Set it Off","Midnight","Alternative/Indie","19"},
	{"Lonely Dance","Set it Off","Midnight","Alternative/Indie","19"},
	{"Different Songs","Set it Off","Midnight","Alternative/Indie","19"},
	{"For You Forever","Set it Off","Midnight","Alternative/Indie","19"},
	{"Dancing With The Devil","Set it Off","Midnight","Alternative/Indie","19"},
	{"Go to Bed Angry","Set it Off","Midnight","Alternative/Indie","19"},
	{"Midnight Thoughts","Set it Off","Midnight","Alternative/Indie","19"},
	{"Criminal Minds","Set it Off","Midnight","Alternative/Indie","19"},
	{"No Disrespect","Set it Off","Midnight","Alternative/Indie","19"},
	{"Stitch Me Up","Set it Off","Midnight","Alternative/Indie","19"},
	{"Raise No Fool","Set it Off","Midnight","Alternative/Indie","19"},
	{"I Want You (Gone)","Set it Off","Midnight","Alternative/Indie","19"},
	{"Unopened Windows","Set it Off","Midnight","Alternative/Indie","19"},
	{"Happy All The Time","Set it Off","Midnight","Alternative/Indie","19"},
	{"Jamie All Over","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Black Cat","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"When I Get Home, You're So Dead","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Jersey","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"If You Wanted a Song Written About You, All You Had to Do Was Ask","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Miserable at Best","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Walk on Water or Drown","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Ocean and Atlantic","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"I'd Hate to Be You When People Find Out What This Song Is About","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Take This to Heart","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"Champagne's for Celebrating (I'll Have a Martini)","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"You Be the Anchor That Keeps My Feet on the Ground, I'll Be the Wings That Keep Your Heart in the Clouds","Mayday Parade","A Lesson in Romantics","Pop-punk","13"},
	{"The 1975","The 1975","The 1975","Indie rock","20"},
	{"The City","The 1975","The 1975","Indie rock","20"},
	{"M.O.N.E.Y.","The 1975","The 1975","Indie rock","20"},
	{"Chocolate","The 1975","The 1975","Indie rock","20"},
	{"Sex","The 1975","The 1975","Indie rock","20"},
	{"Talk!","The 1975","The 1975","Indie rock","20"},
	{"An Encounter","The 1975","The 1975","Indie rock","20"},
	{"Heart Out","The 1975","The 1975","Indie rock","20"},
	{"Settle Down","The 1975","The 1975","Indie rock","20"},
	{"Robbers","The 1975","The 1975","Indie rock","20"},
	{"Girls","The 1975","The 1975","Indie rock","20"},
	{"\"12\"","The 1975","The 1975","Indie rock","20"},
	{"She Way Out","The 1975","The 1975","Indie rock","20"},
	{"Menswer","The 1975","The 1975","Indie rock","20"},
	{"Pressure","The 1975","The 1975","Indie rock","20"},
	{"Is There Somebody Who Can Watch You?","The 1975","The 1975","Indie rock","20"},
	{"May These Noises Startle You in Your Sleep Tonight","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Hell Above","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"A Match into Water","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"King for a Day","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Bulls in the Bronx","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Props & Mayhem","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Tangled in the Great Escape","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"I'm Low on Gas and You Need a Jacket","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"The First Punch","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"One Hundred Sleepless Nights","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Stained Glass Eyes and Colorful Tears","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Hold on Till May","Pierce The Veil","Collide with the Sky","Punk rock","16"},
	{"Entropy","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Cyanide","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Love again","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Frontal Lobe Muzik","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Open up","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Restore the feeling","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Superposition ","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Too deep to turn back","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Complexities ","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"Are you ok? ","Daniel Caesar","Case Study 01 ","R&B/Soul","4"},
	{"3 Nights","Dominic Fike","Don't Forget About Me, Demos","Alternative/Indie","4"},
	{"She Wants My Money","Dominic Fike","Don't Forget About Me, Demos","Alternative/Indie","4"},
	{"Babydoll","Dominic Fike","Don't Forget About Me, Demos","Alternative/Indie","4"},
	{"Westcoast Collective","Dominic Fike","Don't Forget About Me, Demos","Alternative/Indie","4"},
	{"Socks","Dominic Fike","Don't Forget About Me, Demos","Alternative/Indie","4"},
	{"King Of Everything ","Dominic Fike","Don't Forget About Me, Demos","Alternative/Indie","4"},
	{"Intro","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Calvin's Joint","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"The Devil's in the Details","Mac Ayres","Drive Slow","R&B/Soul","13"},
	//800
	{"Slow Down","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Should we take the van? ","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Show me","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Lonely","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Easy ","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Change ya mind","Mac Ayres","Drive Slow","R&B/Soul","13"},
	{"Pasensya Ka Na","Silent Sanctuary","Langit, Luha","Pop","19"},
	{"Rebound ","Silent Sanctuary","Fuchsiang Pag-ibig","Pop","19"},
	{"Kundiman","Silent Sanctuary","Fuchsiang Pag-ibig","Pop","19"},
	{"Sandali Lang","Silent Sanctuary","Fuchsiang Pag-ibig","Pop","19"},
	{"Criminal Minds","Set it Off","Midnight","Alternative/Indie","19"},
	{"No Disrespect","Set it Off","Midnight","Alternative/Indie","19"},
	{"Stitch Me Up","Set it Off","Midnight","Alternative/Indie","19"},
	{"Raise No Fool","Set it Off","Midnight","Alternative/Indie","19"},
	{"I Want You (Gone)","Set it Off","Midnight","Alternative/Indie","19"},
	{"Dreamer's Balls","Queen","Jazz(Queen Album)","Rock","17"},
	{"Fun It","Queen","Jazz(Queen Album)","Rock","17"},
	{"Leaving Home Aint Easy","Queen","Jazz(Queen Album)","Rock","17"},
	{"Dont Stop Me Now","Queen","Jazz(Queen Album)","Rock","17"},
	{"More of that Jazz","Queen","Jazz(Queen Album)","Rock","17"},
	{"Don't Be A Fool","Shawn Mendes","Illuminate","Pop","19"},
	{"Like This ","Shawn Mendes","Illuminate","Pop","19"},
	{"No Promises","Shawn Mendes","Illuminate","Pop","19"},
	{"Lights On","Shawn Mendes","Illuminate","Pop","19"},
	{"Honest ","Shawn Mendes","Illuminate","Pop","19"},
	{"Don't blame me","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Delicate","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Look what you made me do","Taylor Swift","Reputation","Pop-Rock","20"},
	{"So it goes","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Gorgeous","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Getaway car","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Jet Lag","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Astronaut","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Loser of the Year","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Anywhere Else But Here","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Freaking Me Out","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Summer Paradise","Simple Plan","Get Your Heart On!","Pop-punk","19"},
	{"Off The Strength","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"We Just Havent Met Yet","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Always Knew","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Whenever","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Waste My Time","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Inbetween","Russ","Russ The Mixtape","Hiphop/Rap","18"},
	{"Jumpsuit","Twenty One Pilots","Trench","Pop","20"},
	{"Levitate","Twenty One Pilots","Trench","Pop","20"},
	{"Morph","Twenty One Pilots","Trench","Pop","20"},
	{"My Blood","Twenty One Pilots","Trench","Pop","20"},
	{"Chlorine","Twenty One Pilots","Trench","Pop","20"},
	{"Smithereens","Twenty One Pilots","Trench","Pop","20"},
	{"I'm Just A Kid","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"When I'm With You","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Meet You There","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Addicted","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"My Alien","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"God Must Hate Me","Simple Plan","No Pads, No Helmets...Just Balls","Pop-Punk","19"},
	{"Collar Full ","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Far Too Young To Die","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Girl That You Love","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"GirlsGirlsBoys","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Miss Jackson","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Nicotine","Panic! At The Disco","Too Weird To Live, Too Rare To Die!","Pop-Rock","16"},
	{"Endlessly ","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Animal","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Intoxicated ","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Lala","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Her Love Is My Religion ","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Another Me","The Cab","Symphony Soldier ","Alternative/Indie Pop","20"},
	{"Science & Faith","The Script","Science & Faith","Alternative Rock","20"},
	{"Long Gone and Moved on","The Script","Science & Faith","Alternative Rock","20"},
	{"Deadman Walking","The Script","Science & Faith","Alternative Rock","20"},
	{"This Love","The Script","Science & Faith","Alternative Rock","20"},
	{"Walk Away","The Script","Science & Faith","Alternative Rock","20"},
	{"Exit Wounds","The Script","Science & Faith","Alternative Rock","20"},
	{"BeFoUr","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"sHe","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"dRuNk","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"rEaR vIeW","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"wRoNg","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"fOoL fOr YoU","Zayn","Mind of Mine","Pop, R&B, Pop/Rock","26"},
	{"Pressure","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Emergency","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Brighter","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Here We Go Again","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Let This Go","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Woah","Paramore","All We Know Is Falling","Pop-Rock","16"},
	{"Girl Almighty","One Direction","FOUR","Pop music, Pop rock","13"},
	{"Fool’s Gold","One Direction","FOUR","Pop music, Pop rock","13"},
	{"Night Changes","One Direction","FOUR","Pop music, Pop rock","13"},
	{"No Control","One Direction","FOUR","Pop music, Pop rock","13"},
	{"Fireproof","One Direction","FOUR","Pop music, Pop rock","13"},
	{"Spaces","One Direction","FOUR","Pop music, Pop rock","13"},
	//900
	{"Hourglass","Zedd","Clarity","Dance/Electronic","26"},
	{"Shave it","Zedd","Clarity","Dance/Electronic","26"},
	{"Spectrum","Zedd","Clarity","Dance/Electronic","26"},
	{"Lost at Sea","Zedd","Clarity","Dance/Electronic","26"},
	{"Clarity","Zedd","Clarity","Dance/Electronic","26"},
	{"Codec","Zedd","Clarity","Dance/Electronic","26"},
	{"Wilderness","Stephen Speaks","No More Doubt","Pop","19"},
	{"Weather","Stephen Speaks","No More Doubt","Pop","19"},
	{"What'll She Look Like","Stephen Speaks","No More Doubt","Pop","19"},
	{"Filthy","Stephen Speaks","No More Doubt","Pop","19"},
	{"Out of My League","Stephen Speaks","No More Doubt","Pop","19"},
	{"Leaving Song","Stephen Speaks","No More Doubt","Pop","19"},
	{"Wallflower","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Radar (ft. HONNE)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Top Shelf (feat. Bipolar Sunshine)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Good Nights (feat. MASCOLO)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Together","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Superlove (feat. Oh Wonder)","Whethan","Life of a Wallflower Vol.1","Dance/Electronic","23"},
	{"Bawat Kaluluwa","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Not My Energy","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Come Inside Of My Heart","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"The Novel Of My Mind","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Dulo Ng Hangganan","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"In My Prison","Iv of Spades","Clapclapclap!","Alternative","9"},
	{"Battery ","Metallica","Master Of Puppets","Metal","13"},
	{"Damage, Inc.","Metallica","Master Of Puppets","Metal","13"},
	{"Disposable Heroes","Metallica","Master Of Puppets","Metal","13"},
	{"Leper Messiah","Metallica","Master Of Puppets","Metal","13"},
	{"Master Of Puppets","Metallica","Master Of Puppets","Metal","13"},
	{"Orion","Metallica","Master Of Puppets","Metal","13"},
	{"Pakiusap Lang (Lasingin Nyo Ako)","Parokya Ni Edgar","Inuman Session Vol.2","Rock","16"},
	{"Pangarap Lang Kita","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"The Ordertaker (ft. Kamikazee)","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"One Hit Combo","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Bagsakan (ft. Francis M. & Glock-9","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Alumni Homecoming ","Parokya Ni Edgar","Inuman Session Vol.2","Pop","16"},
	{"Eyes Closed","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Alone","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Now or Never","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Sorry","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Bad At Love","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Devil In Me","Halsey","Hopeless Fountain Kingdom","Alternative","8"},
	{"Citezens Of Earth","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"I Hope This Comes Back To Haunt You","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"December","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Smooth Seas Don’t Make Good Sailors","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Cant Kick Up The Roots","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Gold Steps","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"I’ll Be Watching You","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Back To Love","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Eroplanong Papel","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Dive","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Forever","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Breathe Again","December Avenue","December Avenue (2017)","Alternative/Indie","4"},
	{"Dumb Stuff","Lany","Lany","Electropop","12"},
	{"The Breakup","Lany","Lany","Electropop","12"},
	{"Super Far","Lany","Lany","Electropop","12"},
	{"13","Lany","Lany","Electropop","12"},
	{"Overtime","Lany","Lany","Electropop","12"},
	{"Flowers on the Floor","Lany","Lany","Electropop","12"},
	{"It's Time","Imagine Dragons","Night Visions","Alternative","9"},
	{"Demons","Imagine Dragons","Night Visions","Alternative","9"},
	{"On Top of the World","Imagine Dragons","Night Visions","Alternative","9"},
	{"Amsterdam","Imagine Dragons","Night Visions","Alternative","9"},
	{"Hear Me","Imagine Dragons","Night Visions","Alternative","9"},
	{"Every Night","Imagine Dragons","Night Visions","Alternative","9"},
	{"Training Wheels","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Pity Party","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Tag, You're It.","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Milk and Cookies","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Pacify Her","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"Mrs. Potato Head","Melanie Martinez","Cry Baby","Alternative pop; Electropop; Indie pop","13"},
	{"To The End","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"You Know What They Do With Guys Like Us In Prison","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"I'm Not Okay (I Promise)","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"The Ghost of You","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"The Jetset Life is Gonna Kill You","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Interlude","My Chemical Romance","Three Cheers for Sweet Revenge","Alternative, Rock","13"},
	{"Bloom in Love Color","HoneyWorks","HoneyWorks","Rock","8"},
	{"Brazen Honey","HoneyWorks","HoneyWorks","Rock","8"},
	{"Can I confess to you?","HoneyWorks","HoneyWorks","Rock","8"},
	{"The day I knew Love","HoneyWorks","HoneyWorks","Rock","8"},
	{"Declaration of the Weak","HoneyWorks","HoneyWorks","Rock","8"},
	{"Dream Fanfare","HoneyWorks","HoneyWorks","Rock","8"},
	{"03' Adolescence","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"A Tale of 2 Citiez","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Fire Squad","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"St. Tropez","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"G.O.M.D","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"No Role Modelz","J Cole","2014 Forest Hills Drive","Hip-Hop","10"},
	{"Intrapersonal","Turnover","Peripheral Vision","Alternative","20"},
	{"Would Hate You If I Could","Turnover","Peripheral Vision","Alternative","20"},
	{"Like Slowly Disappearing","Turnover","Peripheral Vision","Alternative","20"},
	{"Humming","Turnover","Peripheral Vision","Alternative","20"},
	{"Threshold","Turnover","Peripheral Vision","Alternative","20"},
	{"New Scream","Turnover","Peripheral Vision","Alternative","20"},
	{"Diazepam","Turnover","Peripheral Vision","Alternative","20"},
	{"Take My Head","Turnover","Peripheral Vision","Alternative","20"},
	{"Cutting My Fingers Off","Turnover","Peripheral Vision","Alternative","20"},
	{"Dizzy On The Comedown","Turnover","Peripheral Vision","Alternative","20"},
	{"Citezens Of Earth","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"I Hope This Comes Back To Haunt You","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"December","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Smooth Seas Don’t Make Good Sailors","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Cant Kick Up The Roots","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Gold Steps","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Kali Ma","Neck Deep","Life's Not Out To Get You","Pop-Punk","14"},
	{"Leaving Song","Stephen Speaks","No More Doubt","Pop","19"},
	{"Feet Wet","Stephen Speaks","No More Doubt","Pop","19"},
	{"Passenger Seat","Stephen Speaks","No More Doubt","Pop","19"},
	{"Communion","Stephen Speaks","No More Doubt","Pop","19"},
	{"Toe The Line","Stephen Speaks","No More Doubt","Pop","19"},
	{"Complete","Stephen Speaks","No More Doubt","Pop","19"},
	{"On My Way","Stephen Speaks","No More Doubt","Pop","19"},
	{"Shut Up!","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Welcome to My Life","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Perfect World","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Thank You","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Me Against the World","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Jump","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Everytime","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Promise","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"One","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Untitled","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Perfect","Simple Plan","Still Not Getting Any","Pop-punk/Alternative Rock","19"},
	{"Ready for it?","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Endgame","Taylor Swift","Reputation","Pop-Rock","20"},
	{"I did something bad","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Don't blame me","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Delicate","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Look what you made me do","Taylor Swift","Reputation","Pop-Rock","20"},
	{"So it goes","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Gorgeous","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Getaway car","Taylor Swift","Reputation","Pop-Rock","20"},
	{"King of my heart","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Dress","Taylor Swift","Reputation","Pop-Rock","20"},
	{"Sing","Ed Sheeran","Divide","Pop","5"},
	{"Don't","Ed Sheeran","Divide","Pop","5"},
	{"Nina","Ed Sheeran","Divide","Pop","5"},
	{"Photograph","Ed Sheeran","Divide","Pop","5"},
	{"Bloodstream","Ed Sheeran","Divide","Pop","5"},
	{"Terenife Sea","Ed Sheeran","Divide","Pop","5"},
	{"Thinking out loud","Ed Sheeran","Divide","Pop","5"},
	{"Eraser","Ed Sheeran","Divide","Pop","5"},
	{"Castle on the hill","Ed Sheeran","Divide","Pop","5"},
	{"Dive","Ed Sheeran","Divide","Pop","5"},
	{"Shape of you","Ed Sheeran","Divide","Pop","5"},
	{"Perfect","Ed Sheeran","Divide","Pop","5"},
	{"Galway Girl","Ed Sheeran","Divide","Pop","5"},
	{"Happier","Ed Sheeran","Divide","Pop","5"},
	{"New man","Ed Sheeran","Divide","Pop","5"},
	{"Hearts don't break around here","Ed Sheeran","Divide","Pop","5"},
	{"What do i know","Ed Sheeran","Divide","Pop","5"},
	{"How do you feel","Ed Sheeran","Divide","Pop","5"},
	{"Supermarket Flowers","Ed Sheeran","Divide","Pop","5"},
	{"Barcelona","Ed Sheeran","Divide","Pop","5"},
	{"Bibia be ye ye","Ed Sheeran","Divide","Pop","5"},
	{"Nancy Mulligan","Ed Sheeran","Divide","Pop","5"},
	{"Save yourself","Ed Sheeran","Divide","Pop","5"},
	{"Welcome To New York","Taylor Swift","1989","Pop","20"},
	{"Blank Space","Taylor Swift","1989","Pop","20"},
	{"Style","Taylor Swift","1989","Pop","20"},
	{"Honest","Bazzi","Cosmic","Pop","2"},
	{"Mirror","Bazzi","Cosmic","Pop","2"},
	{"Gone","Bazzi","Cosmic","Pop","2"},
	{"Fantasy","Bazzi","Cosmic","Pop","2"},
	{"BRB","Bazzi","Cosmic","Pop","2"},
	{"Cartier","Bazzi","Cosmic","Pop","2"},
	{"Beautiful ","Bazzi","Cosmic","Pop","2"},
	{"Mine","Bazzi","Cosmic","Pop","2"},
	{"Changed","Bazzi","Cosmic","Pop","2"},
	{"Somebody","Bazzi","Cosmic","Pop","2"},
	{"There's Nothing Holdin' Back","Shawn Mendes","Illuminate","Pop","19"},
	{"Ruin","Shawn Mendes","Illuminate","Pop","19"},
	{"Mercy","Shawn Mendes","Illuminate","Pop","19"},
	{"Treat You Better","Shawn Mendes","Illuminate","Pop","19"},
	{"Three Empty Words","Shawn Mendes","Illuminate","Pop","19"},
	{"Don't Be A Fool","Shawn Mendes","Illuminate","Pop","19"},
	{"Like This ","Shawn Mendes","Illuminate","Pop","19"},
	{"No Promises","Shawn Mendes","Illuminate","Pop","19"},
	{"Midnight Thoughts","Set it Off","Midnight","Alternative/Indie","19"},
	{"Criminal Minds","Set it Off","Midnight","Alternative/Indie","19"},
	{"No Disrespect","Set it Off","Midnight","Alternative/Indie","19"},
	{"Stitch Me Up","Set it Off","Midnight","Alternative/Indie","19"},
	{"Raise No Fool","Set it Off","Midnight","Alternative/Indie","19"},
	{"I Want You (Gone)","Set it Off","Midnight","Alternative/Indie","19"},
	{"Unopened Windows","Set it Off","Midnight","Alternative/Indie","19"},
	{"Happy All The Time","Set it Off","Midnight","Alternative/Indie","19"},
	{"Here We Go Again","Paramore","All We Know Is Falling","Pop-Rock","16"},
	//1000
	};
	string title, singer, album, genre, alpha;
	for (int x=0;x<1001;x++){
		 title = arr[x][0];
		 singer = arr[x][1];
		 album = arr[x][2];
		 genre = arr[x][3];
		 alpha = arr[x][4];
		list.addNode(title,singer,album,genre,alpha);
	}
	switcher();
}